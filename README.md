# TP - 1

## Self-footprinting

### Host-Os
Pour trouver les informations de la machine, on effectue la commande ``systeminfo``.
```
PS C:\Windows\system32> systeminfo                                                                                         

Nom de l’hôte:                              DESKTOP-TFF16N0                 <- NOM DE LA MACHINE
Nom du système d’exploitation:              Microsoft Windows 10 Famille    <- NOM DE L'OS
Version du système:                         10.0.18362 N/A version 18362    <- VERSION DE L'OS
Fabricant du système d’exploitation:        Microsoft Corporation
Configuration du système d’exploitation:    Station de travail autonome
Type de version du système d’exploitation:  Multiprocessor Free
Propriétaire enregistré:                    gael.t@hotmail.ch
Organisation enregistrée:                   N/A
Identificateur de produit:                  00325-95985-89934-AAOEM
Date d’installation originale:              13.09.2020, 23:44:35
Heure de démarrage du système:              12.10.2020, 09:13:48
Fabricant du système:                       ASUSTeK COMPUTER INC.
Modèle du système:                          G752VS
Type du système:                            x64-based PC                    <- ARCHITECTURE DU PROCESSEUR
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : Intel64 Family 6 Model 94 Stepping 3 GenuineIntel ~2601 MHz
Version du BIOS:                            American Megatrends Inc. G752VS.312, 24.04.2019
Répertoire Windows:                         C:\Windows
Répertoire système:                         C:\Windows\system32
Périphérique d’amorçage:                    \Device\HarddiskVolume3
Option régionale du système:                fr;Français (France)
Paramètres régionaux d’entrée:              fr-ch;Français (Suisse)
Fuseau horaire:                             (UTC+01:00) Bruxelles, Copenhague, Madrid, Paris
Mémoire physique totale:                    32 704 Mo                       <- QUANTITÉ DE RAM
Mémoire physique disponible:                27 065 Mo
Mémoire virtuelle : taille maximale:        37 568 Mo
Mémoire virtuelle : disponible:             29 898 Mo
Mémoire virtuelle : en cours d’utilisation: 7 670 Mo
Emplacements des fichiers d’échange:        C:\pagefile.sys
Domaine:                                    WORKGROUP
Serveur d’ouverture de session:             \\DESKTOP-TFF16N0
Correctif(s):                               20 Corrections installées.
                                            [01]: KB4576947
                                            [02]: KB4497165
                                            [03]: KB4503308
                                            [04]: KB4508433
                                            [05]: KB4515383
                                            [06]: KB4516115
                                            [07]: KB4521863
                                            [08]: KB4524569
                                            [09]: KB4528759
                                            [10]: KB4537759
                                            [11]: KB4538674
                                            [12]: KB4541338
                                            [13]: KB4552152
                                            [14]: KB4559309
                                            [15]: KB4560959
                                            [16]: KB4561600
                                            [17]: KB4565554
                                            [18]: KB4569073
                                            [19]: KB4576751
                                            [20]: KB4574727
Carte(s) réseau:                            3 carte(s) réseau installée(s).
                                            [01]: Intel(R) Dual Band Wireless-AC 8260
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        10.33.3.254
                                                  Adresse(s) IP
                                                  [01]: 10.33.3.6
                                                  [02]: fe80::bda4:3e16:75bd:d4b4
                                            [02]: Realtek PCIe GBE Family Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté
                                            [03]: Bluetooth Device (Personal Area Network)
                                                  Nom de la connexion : Connexion réseau Bluetooth
                                                  État :                Support déconnecté
Configuration requise pour Hyper-V:         Extensions de mode du moniteur d’ordinateur virtuel : Oui
                                            Virtualisation activée dans le microprogramme : Oui
                                            Traduction d’adresse de second niveau : Oui
                                            Prévention de l’exécution des données disponible : Oui
```

Pour obtenir le model de la RAM on effectue la commande ``wmic MemoryChip list full``.
```
PS C:\Windows\system32> wmic MemoryChip list full

BankLabel=BANK 0
Capacity=17179869184
DataWidth=64
Description=Physical Memory
DeviceLocator=ChannelA-DIMM0
FormFactor=12
HotSwappable=
InstallDate=
InterleaveDataDepth=2
InterleavePosition=1
Manufacturer=Samsung
MemoryType=0
Model=                      <- MODEL DE LA RAM
Name=Physical Memory
OtherIdentifyingInfo=
PartNumber=M471A2K43BB1-CRC
PositionInRow=
PoweredOn=
Removable=
Replaceable=
SerialNumber=15706711
SKU=
Speed=2400
Status=
Tag=Physical Memory 0
TotalWidth=64
TypeDetail=128
Version=


BankLabel=BANK 2
Capacity=17179869184
DataWidth=64
Description=Physical Memory
DeviceLocator=ChannelB-DIMM0
FormFactor=12
HotSwappable=
InstallDate=
InterleaveDataDepth=2
InterleavePosition=2
Manufacturer=Samsung
MemoryType=0
Model=                      <- MODEL DE LA RAM
Name=Physical Memory
OtherIdentifyingInfo=
PartNumber=M471A2K43BB1-CRC
PositionInRow=
PoweredOn=
Removable=
Replaceable=
SerialNumber=15706711
SKU=
Speed=2400
Status=
Tag=Physical Memory 2
TotalWidth=64
TypeDetail=128
Version=
```

## Devices
Pour trouver les informations du processeur, on effectue la commande ``Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*``.
```
PS C:\Windows\system32> Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*                        

Name                                      NumberOfCores NumberOfEnabledCore NumberOfLogicalProcessors
----                                      ------------- ------------------- -------------------------
Intel(R) Core(TM) i7-6700HQ CPU @ 2.60GHz             4                   4                         8


```

Pour obtenir les détails des disques, on effectue la commande ``diskpart`` pour obtenir les informations de disques.
On selectionne le dique avec ``sel disk <n>``\ (n : numéro du disque). Puis on effectue la commande ``detail disk`` pour obtenir les informations.
```
DISKPART> sel disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> detail disk

HGST HTS721010A9E630                                  <- NOM DU DISQUE
ID du disque : {008F177A-F64E-11EA-A2A4-E4A7A0434C06}
Type : RAID
État : En ligne
Chemin : 0
Cible : 0
ID LUN : 0
Chemin d’accès de l’emplacement : PCIROOT(0)#PCI(1700)#RAID(P00T00L00)
État en lecture seule actuel : Non
Lecture seule : Non
Disque de démarrage : Non
Disque de fichiers d’échange : Non
Disque de fichiers de mise en veille prolongée : Non
Disque de fichiers de vidage sur incident : Non
Disque en cluster  : Non

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 1     E   Disque E:    NTFS   Partition    931 G   Sain                          <- PARTITIONS + FONCTIONS

DISKPART> sel disk 1

Le disque 1 est maintenant le disque sélectionné.

DISKPART> detail disk

NVMe THNSN5256GPU7 TO                                  <- NOM DU DISQUE
ID du disque : {2E9434D8-436D-42D9-99C5-A27DE0181CF1}
Type : RAID
État : En ligne
Chemin : 1
Cible : 0
ID LUN : 0
Chemin d’accès de l’emplacement : PCIROOT(0)#PCI(1700)#RAID(P01T00L00)
État en lecture seule actuel : Non
Lecture seule : Non
Disque de démarrage : Oui
Disque de fichiers d’échange : Oui
Disque de fichiers de mise en veille prolongée : Non
Disque de fichiers de vidage sur incident : Oui
Disque en cluster  : Non

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 2     C   OS           NTFS   Partition    237 G   Sain       Démarrag          <- PARTITIONS + FONCTIONS
  Volume 3         SYSTEM       FAT32  Partition    260 M   Sain       Système

```

## Users
On obtient les informations des utilisateurs avec la commande ``net users``.
```
PS C:\Windows\system32> net users                                                                                       
comptes d’utilisateurs de \\DESKTOP-TFF16N0

-------------------------------------------------------------------------------
Administrateur           DefaultAccount           gaelt
Invité                   WDAGUtilityAccount
La commande s’est terminée correctement.
```

## Processus
La liste des processus s'obtient avec la commande ``tasklist``.
```
PS C:\Windows\system32> tasklist                                                                                        
Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     1 740 Ko
Registry                       120 Services                   0    30 588 Ko
smss.exe                       476 Services                   0     1 208 Ko
csrss.exe                      708 Services                   0     5 660 Ko
wininit.exe                    796 Services                   0     7 076 Ko
csrss.exe                      804 Console                    1     5 884 Ko
services.exe                   868 Services                   0    14 844 Ko
lsass.exe                      888 Services                   0    21 656 Ko
winlogon.exe                   956 Console                    1    11 908 Ko
svchost.exe                    540 Services                   0     3 980 Ko
svchost.exe                    620 Services                   0    32 884 Ko
WUDFHost.exe                   624 Services                   0    36 172 Ko
fontdrvhost.exe                652 Services                   0     3 892 Ko
svchost.exe                    908 Services                   0    17 328 Ko
svchost.exe                   1060 Services                   0     8 860 Ko
fontdrvhost.exe               1140 Console                    1     8 364 Ko
dwm.exe                       1228 Console                    1    78 224 Ko
svchost.exe                   1300 Services                   0     9 348 Ko
svchost.exe                   1316 Services                   0    12 984 Ko
svchost.exe                   1332 Services                   0    11 928 Ko
svchost.exe                   1444 Services                   0    12 536 Ko
svchost.exe                   1456 Services                   0    11 044 Ko
WUDFHost.exe                  1504 Services                   0     5 500 Ko
[...]
```
5 services :
* ``svchost.exe`` :
    * Permet de charger les bibliothèques de liens dynamiques. Il est l'hôte des services d'où son nom : ``svc (services) host (hôte)``
* ``lsass.exe`` : (Local Security Authority Subsystem Service)
    * Assure l'identification des utilisateurs windows sur ce système local.
* ``wininit.exe`` :
    * winint comme son nom l'indique permet l'initialisation de windows (**Win**dows **Init**ialization)
* ``smss.exe`` : (Session Manager Subsystem)
    * smss.exe est lancé dés le processus de démarrage windows. Il permet de vérifier la hiérarchie des fichiers systèmes et de créer des variables d'environnements. Enfin une fois les variables créer il démarre plusieurs processus tel que la gestion de mémoire, le mode noyau du sous system win32 et la connexion à l'utilisateur avec csrss.exe.
* ``csrss.exe`` : (Client/Server Runtime Subsystem)
    * Il permet de gérer les éléments graphiques de windows (fenêtres).

## Network

On obtient les informations des cartes réseaux avec la commande ``Get-NetAdapter`` :
```
PS C:\Windows\system32> Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Connexion réseau Bluet... Bluetooth Device (Personal Area Netw...      13 Disconnected E4-A7-A0-43-4C-06         3 Mbps
Wi-Fi                     Intel(R) Dual Band Wireless-AC 8260           8 Up           E4-A7-A0-43-4C-02       300 Mbps
Ethernet                  Realtek PCIe GBE Family Controller            6 Disconnected 70-8B-CD-1C-D0-74          0 bps
```
* Bluetooth Device (Personal Area Netw... :
    * Il s'agit de la carte permettant connexion Bluetooth. On voit qu'elle est déconnectée et qu'elle a une vitesse de transfert de 3 Mbps.
* Intel(R) Dual Band Wireless-AC 8260 :
    * Cette carte permet la connexion wi-fi, soit la connexion à internet sans fil. Elle est activée et possède une vitesse de transfet de 300 Mbps.
* Realtek PCIe GBE Family Controller :
    * Elle permet la connexion direct en RJ-45 à internet. Elle est déconnectée.

Pour obtenir les port TCP actifs, on effectue la commande ``Get-NetTCPConnection`` :
```
PS C:\Windows\system32> Get-NetTCPConnection

LocalAddress                        LocalPort RemoteAddress                       RemotePort State       AppliedSetting OwningProcess
------------                        --------- -------------                       ---------- -----       -------------- -------------
::                                  50325     ::                                  0          Bound                      12532
::                                  50169     ::                                  0          Bound                      13936
::                                  50115     ::                                  0          Bound                      13464
::                                  49801     ::                                  0          Bound                      4844
::                                  49798     ::                                  0          Bound                      4844
2a01:cb19:792:5700:e527:9cbd:e16... 50325     2a00:1450:4007:812::2008            443        Established Internet       12532
2a01:cb19:792:5700:e527:9cbd:e16... 50169     2600:1901:1:e52::                   443        Established Internet       13936
2a01:cb19:792:5700:e527:9cbd:e16... 50115     2603:1026:100:1::2                  443        Established Internet       13464
2a01:cb19:792:5700:e527:9cbd:e16... 49801     2603:1026:100:1b::2                 443        Established Internet       4844
::1                                 49684     ::                                  0          Listen                     14876
::                                  49670     ::                                  0          Listen                     872
::                                  49668     ::                                  0          Listen                     3636
::                                  49667     ::                                  0          Listen                     2472
::                                  49666     ::                                  0          Listen                     1896
::                                  49665     ::                                  0          Listen                     800
::                                  49664     ::                                  0          Listen                     892
::                                  38068     ::                                  0          Listen                     4
::                                  445       ::                                  0          Listen                     4
::                                  135       ::                                  0          Listen                     1008
0.0.0.0                             50312     0.0.0.0                             0          Bound                      2224
0.0.0.0                             50174     0.0.0.0                             0          Bound                      13936
0.0.0.0                             50173     0.0.0.0                             0          Bound                      13936
0.0.0.0                             50165     0.0.0.0                             0          Bound                      13936
0.0.0.0                             49732     0.0.0.0                             0          Bound                      4712
0.0.0.0                             65530     0.0.0.0                             0          Listen                     15032
192.168.1.20                        50330     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50329     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50328     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50327     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50326     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50324     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50323     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50322     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50321     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50320     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50319     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50318     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50317     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50316     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50315     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50314     52.155.94.78                        443        TimeWait                   0
192.168.1.20                        50313     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50312     40.90.137.124                       443        Established Internet       2224
192.168.1.20                        50311     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50310     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50309     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50308     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50306     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50305     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50304     93.184.216.34                       80         TimeWait                   0
192.168.1.20                        50174     185.199.108.153                     443        Established Internet       13936
192.168.1.20                        50173     185.199.111.153                     443        Established Internet       13936
192.168.1.20                        50165     162.159.136.234                     443        Established Internet       13936
192.168.1.20                        49732     40.67.251.132                       443        Established Internet       4712
0.0.0.0                             49670     0.0.0.0                             0          Listen                     872
0.0.0.0                             49668     0.0.0.0                             0          Listen                     3636
0.0.0.0                             49667     0.0.0.0                             0          Listen                     2472
0.0.0.0                             49666     0.0.0.0                             0          Listen                     1896
0.0.0.0                             49665     0.0.0.0                             0          Listen                     800
0.0.0.0                             49664     0.0.0.0                             0          Listen                     892
0.0.0.0                             45769     0.0.0.0                             0          Listen                     10924
127.0.0.1                           9990      0.0.0.0                             0          Listen                     4336
0.0.0.0                             6646      0.0.0.0                             0          Listen                     5624
127.0.0.1                           6463      0.0.0.0                             0          Listen                     7156
127.0.0.1                           5939      0.0.0.0                             0          Listen                     4732
0.0.0.0                             5040      0.0.0.0                             0          Listen                     12492
192.168.1.20                        139       0.0.0.0                             0          Listen                     4
0.0.0.0                             135       0.0.0.0                             0          Listen                     1008
```

## Scripting

## Gestion de softs

L'intérêt d'utiliser un gestionnaire de paquets est d'augmenter la vitesse de téléchargement d'un logiciel comparer au téléchargement par navigateur,
on a plus besoin de parcourir sur internet pour les avoir.
L'identité des personnes est protégée car on passe par des serveurs tiers.
Sur la sécurité, les logiciels n'ont pas de spyware ou malware.

Sur windows, on utilise ``choco list -l`` pour obtenir la liste des paquets installés.
Pour obtenir les infos d'un paquet, on effectue ``choco info <paquet>``. On peut ainsi connaitre sa provenance.

## Machine Virtuelle

Pour commencer le partage, on doit effectuer la commande : ``New-SmbShare -Name "name" -Path "path"`` pour créer un nouveau partage avec Samba.

Pour accéder à un fichier partager depuis la VM :
    - On fait une connexion ssh sur powershell avec ``ssh root@192.168.120.50``
    - On fait la commande ``mount -t cifs -o username=<UserName>,password=<PassWord> //192.168.1.10/share /opt/partage``
Enfin on partage des fichiers entre notre Os (Windows) avec notre VM sur CentOs.