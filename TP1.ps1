﻿# Gaël Tarendeau
# 19/10/2020

# Clear console command
clear

# Create variables
$PingIP = "8.8.8.8"

$GetOsInfo = Get-WmiObject win32_operatingsystem | select csname, version, caption
$GetIp = Get-NetIPAddress -AddressFamily IPv4 | select ipaddress
$GetStartup = Get-CimInstance -ClassName win32_operatingsystem | select lastbootuptime
$GetPing = (Test-Connection -ComputerName $PingIP -Count 1  | measure-Object -Property ResponseTime -Average).average

#$used_disk = [math]::Round(($total_disk - $free_disk), 1)

# Ram
$RAMinfo = Get-CIMInstance Win32_OperatingSystem | Select FreePhysicalMemory,TotalVisibleMemorySize
$cs = get-wmiobject -class "Win32_ComputerSystem"
$Mem = [math]::Ceiling($cs.TotalPhysicalMemory / 1024 / 1024 / 1024)
$ramfree = $RAMinfo.FreePhysicalMemory * 1.0E-6

# Ram Calculation
$UsedRam = $Mem - $ramfree

#Disk
$diskSpace = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" | Measure-Object Size -Sum).sum /1gb)
$diskFree = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" | Measure-Object FreeSpace -Sum).sum /1gb)
$diskUsed = [math]::Round($diskSpace-$diskFree)


# Making var output
$Name = 'Name : ' + $GetOsInfo.csname
$IP = 'IP : ' + $GetIp.ipaddress[2]
$OS = 'OS : ' + $GetOsInfo.caption + ' | Version: ' + $GetOsInfo.version
$wversion = '10.0.19041'
If ($GetOsInfo.version -eq $wversion)
{
    $out = 'true' 
}
Else
{
    $out = 'false' 
}
$IsOSUTD = 'Is OS up-to-date : ' + $out

$Time = 'Up since : ' + $GetStartup.lastbootuptime

$URam = ' - Used : ' + $UsedRam + ' Gi'
$FRam = ' - Free : ' + $ramfree + ' Gi'

$Dtotal = ' - Total : ' + $diskSpace + ' Go'
$DFree = ' - Free : ' + $diskFree + ' Go'
$DUsed = ' - Used : ' + $diskUsed + ' Go'


$Ping = 'Ping (IP : ' + $PingIP + ') : ' + $GetPing + ' ms'


# Displaying

Write-Output $Name,$IP,$OS,$IsOSUTD,$Time,`n'RAM :',$URam,$FRam, `n'Disk C:\ :',$Dtotal,$DFree,$DUsed,`n$Ping

#FILE TO DOWNLOAD
$downloadurl = "http://dl.google.com/android/installer_r20.0.1-windows.exe"
$UploadURL = "http://www.csm-testcenter.org/test?do=show&subdo=common&test=file_upload"

#SIZE OF SPECIFIED FILE IN MB (adjust this to match the size of your file in MB)
$size = 100

#WHERE TO STORE DOWNLOADED FILE
$documents = [Environment]::GetFolderPath("MyDocuments")
$localfile = "$documents/100mb.bin"


#RUN DOWNLOAD
Write-Output 'Calculating download speed...'
$downloadstart_time = Get-Date
$webclient = New-Object System.Net.WebClient
$webclient.DownloadFile($downloadurl, $localfile)

#CALCULATE DOWNLOAD SPEED
$downloadtimetaken = $((Get-Date).Subtract($downloadstart_time).Seconds)
$downloadspeed = [math]::Round(($size / $downloadtimetaken)*8, 1)
Write-Output "Time taken: $downloadtimetaken second(s)   |   Download Speed: $downloadspeed mbps"

#___________________________________________________________________

#RUN UPLOAD
Write-Output 'Calculating upload speed...'
$uploadstart_time = Get-Date
$webclient.UploadFile($UploadURL, $localfile) > $null;

#CALCULATE UPLOAD SPEED
$uploadtimetaken = $((Get-Date).Subtract($uploadstart_time).Seconds)
$uploadspeed = [math]::Round(($size / $uploadtimetaken) * 8, 1)
Write-Output "Time taken: $uploadtimetaken second(s)   |   Upload Speed: $uploadspeed mbps" 

#___________________________________________________________________

#DELETE TEST DOWNLOAD FILE
Remove-Item –path $localfile
Write-Output `n
pause